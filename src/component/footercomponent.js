import React from 'react';

const Footercomponent = () => {
  return (
    <footer style={{ backgroundColor: '#771011', color: 'white', padding: '10px', textAlign: 'center' }}>
      <p>&copy; 2024 Modest Dress. All rights reserved.</p>
    </footer>
  );
};

export default Footercomponent;
