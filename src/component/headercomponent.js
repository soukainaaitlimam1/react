import React from 'react';
//import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const Headercomponent = () => {
  return (
    <header className=" py-5" style={{backgroundColor: '#771011'}}>
    <div className="container px-4 px-lg-5 my-5">
        <div className="text-center text-white">
            <h1 className="display-4 fw-bolder">Modest DRESS</h1>
            <p className="lead fw-normal text-white-50 mb-0">Every Where Elegent</p>
        </div>
    </div>
</header>
  );
};

export default Headercomponent;
