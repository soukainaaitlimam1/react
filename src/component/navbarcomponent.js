//import React, { useState } from 'react';
import { Navbar, Nav, NavDropdown, Container } from 'react-bootstrap';
import { FaShoppingCart } from 'react-icons/fa';
import 'bootstrap/dist/css/bootstrap.min.css';

const Navbarcomponent = ({ cartCount }) => {
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="#home">Modest Dress</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#about">About</Nav.Link>
            <NavDropdown title="Shopping" id="basic-nav-dropdown">
              {/* Add your product links here */}
              <NavDropdown.Item href="#product1">Modest Dress 1</NavDropdown.Item>
              <NavDropdown.Item href="#product2">Modest Dress 2</NavDropdown.Item>
              <NavDropdown.Item href="#product3">Modest Dress 3</NavDropdown.Item>
              <NavDropdown.Item href="#product4">Modest Dress 4</NavDropdown.Item>
              <NavDropdown.Item href="#product5">Modest Dress 5</NavDropdown.Item>
              <NavDropdown.Item href="#product6">Modest Dress 6</NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Nav>
            {/* Cart button with shopping cart icon and counter */}
            <div>
              <button className="btn btn-outline-dark" >
                <FaShoppingCart />
                Cart
                <span className="badge bg-dark text-white ms-1 rounded-pill">{cartCount}</span>
              </button>
            </div>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Navbarcomponent;
