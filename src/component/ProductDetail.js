import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Link } from 'react-router-dom';

const ProductDetail = ({ products, addToCart, setQuantity }) => {
  const [quantity, setLocalQuantity] = useState(""); // Initialize local quantity state
  const [message, setMessage] = useState("");

  const { id } = useParams();
  const product = products.find((p) => p.id === parseInt(id, 10));

  const handleQuantityChange = (e) => {
    setLocalQuantity(e.target.value);
  };

  const handleAddToCart = (e) => {
    e.preventDefault();
    setQuantity(quantity); // Set the quantity in the common ancestor (App)
    setMessage(`${quantity} are added`);
    addToCart({ ...product, quantity: parseInt(quantity, 10) || 0 });
    setLocalQuantity(""); // Clear the local quantity after adding to the cart
  };

  useEffect(() => {
    setQuantity(quantity); // Update the quantity in the common ancestor (App) after the state is guaranteed to be updated
  }, [quantity, setQuantity]);

  if (!product) {
    return <div>Product not found</div>;
  }

  return (
    <div className="container mt-5">
      <div className="row">
        <div className="col-md-6">
          <img src={product.image} alt={product.title} className="img-fluid" style={{ width: '100%' }} />
        </div>
        <div className="col-md-6">
          <div style={{ backgroundColor: '#f8f9fa', padding: '20px', borderRadius: '10px' }}>
            <h2 className="text-dark">{product.title}</h2>
            <p className="lead text-dark">Description: {product.description}</p>
            <p className="text-muted">Price: ${product.price}</p>

            {/* Simple Form for Demonstration */}
            <form>
              <div className="mb-3">
                <label htmlFor="quantity" className="form-label text-dark">
                  Quantity:
                </label>
                <input
                  type="number"
                  className="form-control"
                  id="quantity"
                  placeholder="Enter quantity"
                  value={quantity}
                  onChange={handleQuantityChange}
                />
              </div>
              <button
                type="submit"
                className="btn btn-primary"
                style={{ backgroundColor: '#771011', border: 'solid #771011' }}
                onClick={handleAddToCart}
              >
                Add to Cart
              </button>
              {parseInt(quantity, 10) > 0 && <p className="mt-2">{message}</p>}
            </form>

            {/* Button to Go Back to Home Page */}
            <Link to="/" className="btn btn-secondary mt-3">
              Go Back to Home Page
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
