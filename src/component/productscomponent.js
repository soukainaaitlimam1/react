import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Card } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const Productscomponent = ({ products }) => {
  return (
    <Container className="mt-3">
      <Row>
        {products.map((product) => (
          <Col key={product.id} md={4}>
            <Card className="mb-3">
              <Link to={`/product/${product.id}`} style={{ textDecoration: 'none', color: 'black' }}>
                <Card.Img variant="top" src={product.image} alt={product.title} />
                <Card.Body>
                  <Card.Title>{product.title}</Card.Title>
                  <Card.Text>{product.description}</Card.Text>
                  <Card.Text>${product.price}</Card.Text>
                </Card.Body>
              </Link>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default Productscomponent;
