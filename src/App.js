import Headercomponent from './component/headercomponent';
import Navbarcomponent from './component/navbarcomponent';
import Productscomponent from './component/productscomponent';
import { useState } from 'react';
import Footercomponent from './component/footercomponent';
import image1 from './images/image1.jpg';
import image2 from './images/image2.jpg';
import image3 from './images/image3.jpg';
import image4 from './images/image4.jpg';
import image5 from './images/image5.jpg';
import image6 from './images/image6.jpg';
import 'bootstrap/dist/css/bootstrap.min.css';
//import Textfield from "./Forms/Textfield";

function App() {
    const [cart, setCart] = useState([]);
    const [cartCount, setCartCount] = useState(0);
  
    const addToCart = (product) => {
      setCart([...cart, product]);
      setCartCount(prevCount => prevCount + 1);
    }
    const sampleProducts = [
      { id: 1, title: 'Modest Dress 1', description: 'Description of Modest dress 1', price: 50.00, image: image1 },
      { id: 2, title: 'Modest dress 2', description: 'Description of Modest dress 2', price: 30.00, image: image2 },
      { id: 3, title: 'Modest dress 3', description: 'Description of Modest dress 3', price: 40.00, image: image3 },
      { id: 4, title: 'Modest dress 1', description: 'Description of Modest dress 1', price: 50.00, image: image4 },
      { id: 5, title: 'Modest dress 2', description: 'Description of Modest dress 2', price: 30.00, image: image5 },
      { id: 6, title: 'Modest dress 3', description: 'Description of Modest dress ', price: 40.00, image: image6 },
    ];
    return(
        // <div>
        //     {/* <Textfield></Textfield> */}
        // </div>
   
    
        <div>
        <Navbarcomponent cartCount={cartCount}></Navbarcomponent>
       <Headercomponent></Headercomponent>
       <Productscomponent products={sampleProducts} addToCart={addToCart}></Productscomponent>
       <Footercomponent></Footercomponent>
       </div> )
      
    };
    export default App ;