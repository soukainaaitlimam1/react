// import { useState } from "react";
// import './textfeild.css'; 
// function Inscription({username}) {
//     const [nom, setNom] = useState("")
//     const [password, setPassword] = useState("")
//     const [dateNaissance, setDateNaissance] = useState("")
//     const [ville, setVille] = useState("")
//     const [genre, setGenre] = useState("")
//     const [loisirs, setLoisirs] = useState([])
//     const [photo, setPhoto] = useState(null);
//     const [reussi, setReussi] = useState(false);
//     const [message, setMessage] = useState('');
//     const handlePhotoChange = (e) => {
//         if (e.target.files && e.target.files.length > 0) {
//           setPhoto(e.target.files[0]);
//         }
//       };
//     function handleLoisirs(e) {
//         if(!loisirs.includes(e.target.value)) {
//             setLoisirs([...loisirs, e.target.value]);
//         } else {
//             setLoisirs([...loisirs.filter((item)=>item!==e.target.value)]);
//         }
//     }
//     const handleSubmit = (e) => {
//         e.preventDefault();
//         setReussi(true);
//         setMessage(`Je suis ${username} né le ${dateNaissance} à ${ville} et mes loisirs sont : ${loisirs.join(', ')}`);
//       };
//     return(
//         < div className="container">
//         <h1>Formulaire</h1>
//         <div className="full">
            
//         <form onSubmit={(e)=>handleSubmit(e)}>
//             <p>
//                 <label>Date de naissance</label>
//                 <input type="date" name="dateNaissance" onChange={(e)=>setDateNaissance(e.target.value)}/>
//             </p>
//             <p>
//                 <label>Ville</label>
//                 <select name="ville"  onChange={(e)=>setVille(e.target.value)}>
//                     <option>Choisir une ville</option>
//                     <option value="Agadir">Agadir</option>
//                     <option value="Tiznit">Tiznit</option>
//                 </select>
//             </p>
//             <p>
//                 <label>Genre</label>
//                 <input type="radio" name="genre" value="Homme" onChange={(e)=>setGenre(e.target.value)}/>Homme
//                 <input type="radio" name="genre" value="Femme" onChange={(e)=>setGenre(e.target.value)}/>Femme
//             </p>
//             <p>
//                 <label>Loisirs</label>
//                 <input type="checkbox" name="loisirs" value="Sport" onChange={(e)=>handleLoisirs(e)}/>Sport
//                 <input type="checkbox" name="loisirs" value="Lecture" onChange={(e)=>handleLoisirs(e)}/>Lecture
//                 <input type="checkbox" name="loisirs" value="Musique" onChange={(e)=>handleLoisirs(e)}/>Musique
//             </p>
//             <p>
//                 <label >Photo</label>
//                 <input type="file" onChange={handlePhotoChange} />
//             </p>
//             <p>
//                 <input type="submit" value="S'inscrire"  className="button1"/>
//             </p>
//         </form>
        
//         </div>{reussi && (<div>
//             <h2>Informations</h2>
//             <table border={1}>
//               <tbody>
//                 <tr>
//                   <td>Nom:</td>
//                   <td>{username}</td>
//                 </tr>
//                 <tr>
//                   <td>Date de Naissance:</td>
//                   <td>{dateNaissance}</td>
//                 </tr>
//                 <tr>
//                   <td>Ville:</td>
//                   <td>{ville}</td>
//                 </tr>
//                 <tr>
//                   <td>Genre :</td>
//                   <td>{genre}</td>
//                 </tr>
//                 <tr>
//                   <td>Loisirs :</td>
//                   <td>{loisirs}</td>
//                 </tr>
//                 <tr>
//                   <td>Image:</td>
//                   <td>{photo}</td>
//                 </tr>
//               </tbody>
//             </table>
//           </div>)}</div>
//     )
// }
// export default Inscription;
